# PPM Web Viewer

Viewing PBM, PGM, and PPM images in the browser. 

> By Andrew Pankow

More Information:

* [PPM Wikipedia Page](https://en.wikipedia.org/wiki/Netpbm_format)
* [PPM Specification Page](http://netpbm.sourceforge.net/doc/ppm.html)


## Standard Handling of PPM Files

Features include:

* PBM (black and white), PGM (grayscale), PPM (255-color)
  * `P1`, `P2`, `P3`, `P4`, `P5`, `P6`
  * Some examples at the bottom of this document
* Width and Height based on Image Data
* `<canvas>` `class` set to `ppm-img` for ease of manipulation
* `<canvas>` `id` set to image(s)\' filename

![Handling A PPM File](docs/example-uploads.png "Handling A PPM File")


## How to Use

```js
var images = [];

function drawPPM(data,imageName) {
	/* Make PBM, PGM, or PPM image */
	var ppm = new ppmImage(imageName).load(data);
	var image = ppm.img;
	/**
	* if image did not load or is still blank, can be forced to be displayable:
	*    var image = ppm.draw(true)
	* instead of 
	*    var image = ppm.img
	**/

	if(image) { /*Precaution against blank images*/
		
		/* Render image to screen */
		document.getElementById("gallery").appendChild(image);
		
		/*--or--*/

		/* Save displayable images*/
		images.push( ppm );
	} else {
		images.push( ppm ); /*include non-displayable images for debugging*/
}

console.log(images);
```

## API

Initiating an Image	
```js
	constructor(name="")

	// examples
	var ppm = new ppmImage();
	var ppm = new ppmImage("theImageTitle");
```

Loading Image Data
```js
	load(ppmData)

	// example
	ppm.load("P3 3 2 1  1 0 0   0 1 0   0 0 1  1 1 0   1 1 1   0 0 0");
```

Image Sizes

> WARNING: Really only for viewing and loading purposes. If resizing final render, take caution.

```js
	.width
	.height

	// example
	console.log(ppm.width, ppm.height);
	// 1920 1280
```

Image Types
```js
	.format
	.type

	// examples
	console.log(ppm.format);
	// "pbm-image"
...
	// "pgm-image"
...
	// "ppm-image"

	console.log(ppm.type);
	// "P1"
...
	// "P2"
...
	// "P3"
...
	// "P4"
...
	// "P5"
...
	// "P6"
```

Image Names

```js
	.name
	setName(newName="")

	// examples
	console.log(ppm.name);
	// "ppm-img"

	/* ppm.name = "someNewName" wont work. There is a validation stage under the hood. */
	ppm.setName("someNewName");

	console.log(ppm.name);
	// "someNewName"
```

Getting the Image to the Page 

```js
	.img /*an alias for draw(false)*/
	draw(force=false)

	// examples
	document.getElementById("gallery").appendChild(ppm.img);
	document.getElementById("gallery").appendChild(ppm.draw());

	//example for including any blank images
	document.getElementById("gallery").appendChild(ppm.draw(true));
```

## Examples

P1 - An ASCII Black & White PBM

![Handling An ASCII PBM](docs/example-p1.png "Handling An ASCII PBM")

P2 - An ASCII Greyscale PGM

![Handling An ASCII PGM](docs/example-p2.png "Handling An ASCII PGM")

P3 - An ASCII Color PPM

![Handling An ASCII PPM](docs/example-p3.png "Handling An ASCII PPM")

P6 - A Binary Color PPM

![Handling A Binary PPM](docs/example-p6.png "Handling A Binary PPM")