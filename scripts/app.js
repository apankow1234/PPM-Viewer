function count(name, custom_regex=null) {
	let default_regex = new RegExp("^("+name+")(?:[\\-]\d+)?", "i");
	let existsRegex = (custom_regex ? custom_regex : default_regex);
	return Array.prototype.slice
		.call(document.getElementsByTagName("*"))
		.map(function(el) {
			return el.id;
		})
		.filter(function(ident) {
			return (existsRegex.test(ident));
		})
		.length;
}

var images = [];
function drawPPM(data, imageName, allowDuplicates=false) {
	/*
		Make PBM, PGM, or PPM image
	*/
	let ppm = new ppmImage().setName(imageName).load(data);
	let image = ppm.img;

	if(image) {
		/*
			Deal with duplicate IDs on page
		*/
		let instances = count(ppm.name);
		if(parseInt(instances) > 0) {
			if(allowDuplicates) {
				image.setAttribute('id', (image.id+"-"+instances));
			} else {
				console.log("Sorry: `"+ppm.name+"` was already in use as an ID on the page.");
				return;
			}
		}

		/*
			Render image to screen
		*/
		document.getElementById("gallery").appendChild(image);
		images.push( ppm ); /*include displayable images*/
	} else {
		images.push( ppm ); /*include non-displayable images*/
	}
}

function getFiles(evt) {
	for(let i = 0; i < evt.target.files.length; i++) {
		/* Each file is evt.target.files[i]; */
		if(evt.target.files[i] != null) {
			let r = new FileReader();
			r.onload = (function(dataFile) {
				return function(e) {
					/*
						Do something with `e.target.result` (the file data)
					*/
					drawPPM(e.target.result, evt.target.files[i].name, true);
				}
			})(evt.target.files[i]);
			r.readAsArrayBuffer(evt.target.files[i]);
		}
	}
}

document.getElementById('ppm-imgs').addEventListener('change', getFiles, false);