/**
 * @summary A tool to read PBM, PGM, and PPM images to the canvas
 * @author Andrew Pankow <apankow1234@gmail.com>
 */
var verbose = true;
class ppmImage
{
	constructor(name="") {
		this.__name   = "ppm-img";
		this.__type   = 3;
		this.__fmt    = 'ppm-img';
		this.__pixels = {};
		if(name)
			this.setName(name);
	}

	get name() {
		return this.__name;
	}

	get height() {
		return Object.keys(this.__pixels).length;
	}

	get width() {
		return Object.keys(this.__pixels[0]).length;
	}

	get format() {
		return this.__fmt;
	}

	get type() {
		return "P"+this.__type;
	}

	get img() {
		return this.draw();
	}

	setName(newName="") {
		if(newName) {
			var extRegex = /[\.]p[pgb]m$/i;
			if(newName.match(extRegex))
				this.__name = newName.substring(0, newName.lastIndexOf("."));
			else {
				this.__name = newName;
			}
		} else {
			if(verbose)
				console.log("Image could not acquire a unique name");
			return;
		}
		this.__name = this.__name.replace(/\s/g,'_');
		return this;
	}

	reset() {
		this.__pixels = {};
		return this;
	}

	load(ppmData) {
		/*
			Comb through the data
		*/
		let decoder = new TextDecoder('utf-8');
		let conv = decoder.decode(ppmData);

		if(!conv.substring(0,2).match(/^[pP][1-6]/)) {
			if(verbose)
				console.log("Sorry: this data was not a properly formatted PBM, PGM, nor PPM");
			this.reset();
			return this;
		}
		this.__type = conv[1];
		if(this.__type.match(/[456]/)) {
			let metadataRegex = /(^[pP].*?\s*.*?\d[\d\s]*\d[\n\r]+)/igm;
			let meta = conv.match(metadataRegex)[0];

			let encoder = new TextEncoder();
			let binData = new Uint8Array(ppmData.slice(encoder.encode(meta).length));
			conv = (meta + binData.join(' '));
		}
		ppmData = null; /*Files can be huge! Trim the fat wherever possible.*/

		var allLines = conv.substring(2).split(/\n/);
		conv         = null; /*Files can be huge! Trim the fat wherever possible.*/
		var data     = allLines.filter(function(line) {
				return (line.length > 0 && !line.startsWith('#')); /*Skip commented and blank lines*/
			}).join().trim().split(/[\s\,]+/).map(function(el){return parseInt(el);});
		allLines = null; /*Files can be huge! Trim the fat wherever possible.*/

		/*
			Prepare Image
		*/
		// this.resize(data[0], data[1]);
		var width  = data[0];
		var height = data[1];

		var inc = ((this.__type == 3 || this.__type == 6) ? 3 : 1); /*values per pixel in data*/
		var fmt, depth, init;
		if(this.__type == 1 || this.__type == 4) {
			init    =  2;        /*only the height and width are present in the data*/
			depth   = -1;       /*represents on and off (negative because `on` will be black and `off` will be white)*/
			fmt     = 'b';      /*black & white image*/
		} else {
			init    = 3;        /*the color bit-depth follows the height and width in the data*/
			depth   = data[2];  /*color bit-depth based on data's specification*/
			if(this.__type == 2 || this.__type == 5)
				fmt = 'g';      /*greyscale image*/
			else
				fmt = 'p';      /*color image*/
		}
		this.__fmt = 'p'+fmt+'m-img';
		var values = data.slice(init,data.length).map(function(el){
			return parseInt(( el / depth ) * 255);
		});
		data = null; /*Files can be huge! Trim the fat wherever possible.*/

		/*
			Get Each Pixel
		*/
		for(var i=0; i < values.length; i+=inc) {
			let id = Math.floor(i / inc);
			let x  = id % width;
			let y  = Math.floor(id / width);
			let px = [];
			if(this.__type == 3 || this.__type == 6) {
				px = values.slice(i,i+3);
			}
			else {
				if(this.__type == 1 || this.__type == 4)
					values[i] = 255 + values[i]; /*offset because we inverted the depth for the b&w images*/
				px = [values[i], values[i], values[i]];
			}

			if(!this.__pixels.hasOwnProperty(y))
				this.__pixels[y] = [];
			this.__pixels[y][x] = px;
		}
		return this;
	}

	draw(force=false) {
		if(force || Object.keys(this.__pixels).length) {
			/*
				Setup canvas for image
			*/
			var c = document.createElement('canvas');
			c.setAttribute('class',  this.__fmt);
			c.setAttribute('width',  this.width);
			c.setAttribute('height', this.height);
			c.setAttribute('id',     this.__name);
			c.setAttribute('name',   this.__name);
			/*
				Paint Each Pixel
			*/
			var ctx = c.getContext('2d');
			for(var y in this.__pixels) {
				for(var x in this.__pixels[y]) {
					let px = this.__pixels[y][x];
					ctx.fillStyle = 'rgba('+px[0]+','+px[1]+','+px[2]+',255)';
					ctx.fillRect(x, y, 1, 1);
				}
			}
			return c;
		} else {
			if(verbose)
				console.log("Did not draw blank image.");
			return;
		}
	}
}
